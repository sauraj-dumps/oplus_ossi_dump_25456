#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:0f15ca4148b3e9389df7ebb098c291f06d556358; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:449ac37f308adc6d066f6b25984fde1288f07c9c \
          --target EMMC:/dev/block/by-name/recovery:134217728:0f15ca4148b3e9389df7ebb098c291f06d556358 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
